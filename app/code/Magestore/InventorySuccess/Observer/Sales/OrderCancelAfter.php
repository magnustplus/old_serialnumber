<?php

/**
 * Copyright © 2016 Magestore. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Magestore\InventorySuccess\Observer\Sales;

use Magento\Framework\Event\Observer as EventObserver;
use Magento\Framework\Event\ObserverInterface;


/**
 * Class OrderCancelAfter
 * @package Magestore\DropshipSuccess\Observer\Sales
 */
class OrderCancelAfter implements ObserverInterface
{
    /**
     * @var \Magestore\SerialSuccess\Model\ItemFactory
     */
    protected $serialFactory;

    /**
     * OrderCancelAfter constructor.
     * @param \Magestore\SerialSuccess\Model\ItemFactory $serialFactory
     */
    public function __construct(
        \Magestore\SerialSuccess\Model\ItemFactory $serialFactory
    )
    {
        $this->serialFactory = $serialFactory;
    }

    /**
     *
     * @param EventObserver $observer
     * @return $this
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     */
    public function execute(EventObserver $observer)
    {
        $order = $observer->getEvent()->getOrder();
        $serialString = $order->getData('serial_string');

        foreach (explode(',', $serialString) as $sn) {
            if($sn != null) {
                $serial = $this->serialFactory->create()->load($sn, 'serial');
                if($serial->getId()) {
                    $serial->setData('status', 1)->save();
                }
            }
        }

        return $this;
    }
}