/*
 *  Copyright © 2016 Magestore. All rights reserved.
 *  See COPYING.txt for license details.
 *
 */

/*global define*/
define(
    [
        'jquery',
        'ko',
        'mage/translate',
        'Magestore_Webpos/js/model/sales/order/shipment',
        'Magestore_Webpos/js/model/event-manager',
        'Magestore_Webpos/js/helper/alert',
        'Magestore_Webpos/js/action/notification/add-notification',
        'Magestore_Webpos/js/model/sales/order-factory'
    ],
    function($, ko, $t, shipment, eventmanager, alertHelper, notification, OrderFactory) {
        'use strict';
        return {
            isValid: false,
            orderData: {},
            submitArray: [],
            submitData: {
                "entity":{
                    "orderId": 0,
                    "emailSent": 0,
                    "items": [],
                    "tracks": [],
                    "comments": []
                }
            },
            items: {},
            comment: {},
            track: {},

            execute: function(data, orderData, deferred, parent){
                var self = this;
                this.isValid = false;
                this.orderData = orderData;
                this.submitData = {
                    "entity":{
                        "orderId": this.orderData.entity_id,
                        "emailSent": 0,
                        "items": [],
                        "tracks": [],
                        "comments": []
                    }
                };
                $.each(data, function(index, value){
                    self.submitData = self.bindEmail(self.submitData,value);
                    self.submitData = self.bindItem(self.submitData,value);
                    self.submitData = self.bindTrack(self.submitData,value);
                    self.submitData = self.bindComment(self.submitData,value);
                });
                if(!this.isValid){
                    alertHelper({title:'Error', content: $t('Please choose an item to ship')});
                    return;
                }
                notification($t('The shipment has been created successfully.'), true, 'success', $t('Success'));
                parent.orderData(null);
                parent.display(false);
                if(this.submitData.entity.items.length>0){
                    shipment().setPostData(this.submitData).setMode('online').save();
                    this.saveOrderOffline(this.submitData);
                    this.updateSerialNumber(this.submitData);
                }                
            },

            saveOrderOffline: function(submitData){
                var self = this;
                if(submitData.entity.items.length>0){
                    $.each(self.orderData.items, function(orderItemIndex, orderItemValue){
                        $.each(submitData.entity.items, function(index, value){
                            if(value.orderItemId == orderItemValue.item_id){
                                orderItemValue.qty_shipped += value.qty;
                            }
                            if(value.orderItemId == orderItemValue.parent_item_id){
                                orderItemValue.qty_shipped += value.qty;
                            }
                        });
                    });
                }
                eventmanager.dispatch('sales_order_shipment_afterSave', {'response': this.orderData});
            },

            bindEmail: function(data, item){
                if(item.name.search('send_email')===0 && parseFloat(item.value)>0){
                    data.entity.emailSent = 1;
                }
                return data;
            },

            updateSerialNumber: function(submitData){
                var self = this;
                $.each(submitData.entity.items, function(index, value){
                    var self1 = self;
                    // Add serial_shipping
                    var additionalData = JSON.parse(value.additionalData);
                    if(typeof additionalData.serial_string !='undefined') {
                        var listItems = [];
                        var serial_string = additionalData.serial_string;
                        $.each(self.orderData.items, function(orderItemIndex, orderItemValue){
                            if(value.orderItemId == orderItemValue.item_id) {
                                if(orderItemValue.serial_shipping == null) {
                                    orderItemValue.serial_shipping = ',' + serial_string;
                                } else {
                                    orderItemValue.serial_shipping = orderItemValue.serial_shipping + ',' + serial_string;
                                }
                                listItems.push(orderItemValue);
                            } else {
                                listItems.push(orderItemValue);
                            }
                        });
                        self.orderData.items = listItems;
                        OrderFactory.get().setData(self.orderData).save().done(function () {

                        });
                    }
                });
            },

            bindItem: function(data, item){
                var tmp_qty = 0;
                item.value.split(',').forEach(function (el) {
                    if(el != null && el != '') {
                        tmp_qty++;
                    }
                });
                if(item.name.search('items')===0 && parseFloat(tmp_qty)>0){
                    this.isValid = true;
                    this.item = {};
                    var isExist = false;
                    item.name = item.name.replace("items[", "");
                    item.name = item.name.replace("]", "");
                    $.each(data.entity.items, function(index, value){
                        if(value.orderItemId == item.name){
                            if(typeof value.additionalData !='undefined') {
                                var tmp = JSON.parse(value.additionalData);
                                if(typeof tmp.serial_string != 'undefined') {
                                    if (tmp.serial_string.indexOf(item.value) >= 0) {
                                    } else {
                                        tmp.serial_string = tmp.serial_string + ',' + item.value;
                                        value.qty = parseFloat(value.qty) + 1;
                                    }
                                } else {
                                    tmp.serial_string = item.value;
                                    value.qty = parseFloat(value.qty) + 1;
                                }
                                value.additionalData = JSON.stringify(tmp);
                            } else {
                                value.additionalData = JSON.stringify({serial_string: item.value});
                                value.qty = parseFloat(value.qty) + 1;
                            }
                            isExist = true;
                        }
                    });
                    if(!isExist) {
                        this.item.orderItemId = parseInt(item.name);
                        this.item.qty = 1;
                        this.item.additionalData = JSON.stringify({serial_string: item.value});
                        data.entity.items.push(this.item);
                    }
                }
                return data;
            },

            bindTrack: function(data, item){
                if(item.name.search('tracking')===0 && item.value!=''){
                    this.track = {};
                    this.track.trackNumber = item.value;
                    data.entity.tracks.push(this.track);
                }
                return data;
            },

            bindComment: function(data, item){
                if(item.name.search('comment_text')===0 && item.value){
                    this.comment.comment = item.value;
                    data.entity.comments.push(this.comment);
                }
                return data;
            },
        }
    }
);
