<?php
/**
 *  Copyright © 2016 Magestore. All rights reserved.
 *  See COPYING.txt for license details.
 *
 */

namespace Magestore\Webpos\Setup;

use Magento\Framework\Setup\UpgradeSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Eav\Setup\EavSetupFactory;
use Magento\Eav\Setup\EavSetup;

/**
 * Upgrade the Catalog module DB scheme
 */
class UpgradeSchema implements UpgradeSchemaInterface
{
    /**
     * @var ModuleDataSetupInterface
     */
    protected $moduleDataSetup;
    
    /**
     * @var EavSetupFactory
     */
    protected $eavSetupFactory;
    
    /**
     * UpgradeSchema constructor
     *
     * @param EavSetupFactory $eavSetupFactory
     */
    public function __construct(
        ModuleDataSetupInterface $moduleDataSetup,
        EavSetupFactory $eavSetupFactory
    )
    {
        $this->moduleDataSetup = $moduleDataSetup;
        $this->eavSetupFactory = $eavSetupFactory;
    }
    
    /**
     * {@inheritdoc}
     */
    public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();
        if (version_compare($context->getVersion(), '1.1.0', '<')) {
            //add tax_class_id for sales_order_table table
            $setup->getConnection()->addColumn(
                $setup->getTable('sales_order_item'),
                'custom_tax_class_id',
                array(
                    'type'      => \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                    'nullable'  => true,
                    'length'    => '11',
                    'comment'   => 'Custom Tax Class Id'
                )
            );

            $setup->getConnection()->addColumn(
                $setup->getTable('catalog_product_entity'),
                'updated_datetime',
                \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP,
                null,
                ['nullable' => false, 'default' => \Magento\Framework\DB\Ddl\Table::TIMESTAMP_UPDATE],
                'Updated Time'
            );
            /** @var EavSetup $eavSetup */
            $eavSetup = $this->eavSetupFactory->create(['setup' => $this->moduleDataSetup]);
            /**
             * Remove attribute webpos_visible
             */
            //Find these in the eav_entity_type table
            $action = \Magento\Framework\App\ObjectManager::getInstance()->get(
                '\Magento\Catalog\Model\ResourceModel\Product\Action'
            );
            $attribute = $action->getAttribute('webpos_visible');
            if($attribute){
                $entityTypeId = \Magento\Framework\App\ObjectManager::getInstance()
                ->create('Magento\Eav\Model\Config')
                ->getEntityType(\Magento\Catalog\Api\Data\ProductAttributeInterface::ENTITY_TYPE_CODE)
                ->getEntityTypeId();
                $eavSetup->removeAttribute($entityTypeId, 'webpos_visible');
            }
            
            $eavSetup->removeAttribute(
                \Magento\Catalog\Model\Product::ENTITY,
                'webpos_visible'
            );
            
            /**
            * Add attributes to the eav/attribute
            */
            $eavSetup->addAttribute(
                \Magento\Catalog\Model\Product::ENTITY,
                'webpos_visible',
                [
                    'group' => 'General',
                    'type' => 'int',
                    'backend' => '',
                    'frontend' => '',
                    'label' => 'Enable On Webpos',
                    'input' => 'boolean',
                    'class' => '',
                    'source' => 'Magento\Eav\Model\Entity\Attribute\Source\Boolean',
                    'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_GLOBAL,
                    'visible' => true,
                    'required' => false,
                    'user_defined' => false,
                    'default' => '1',
                    'searchable' => false,
                    'filterable' => false,
                    'comparable' => false,
                    'visible_on_front' => false,
                    'used_in_product_listing' => false,
                    'unique' => false,
                    'apply_to' => ''
                ]
            );
        }
        
        if (version_compare($context->getVersion(), '1.1.1', '<')) {
            //add customer full name for sales_order table
            $setup->getConnection()->addColumn(
                $setup->getTable('sales_order'),
                'customer_fullname',
                array(
                    'type'      => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    'nullable'  => true,
                    'length'    => '255',
                    'comment'   => 'Customer Full Name'
                )
            );
        }
        
        if (version_compare($context->getVersion(), '1.1.2', '<')) {
            $setup->getConnection()->addColumn(
                $setup->getTable('sales_order'),
                'webpos_init_data',
                array(
                    'type'      => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    'nullable'  => true,
                    'comment'   => 'Web POS init data use for on hold order'
                )
            );

            $setup->getConnection()->addColumn(
                $setup->getTable('quote'),
                'webpos_cart_discount_type',
                array(
                    'type'      => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    'nullable'  => true,
                    'length'    => '5',
                    'comment'   => 'Web POS Discount Type'
                )
            );

            $setup->getConnection()->addColumn(
                $setup->getTable('quote'),
                'webpos_cart_discount_value',
                array(
                    'type'      => \Magento\Framework\DB\Ddl\Table::TYPE_DECIMAL,
                    'nullable'  => true,
                    'length'    => '12,4',
                    'comment'   => 'Web POS Discount Value'
                )
            );

            $setup->getConnection()->addColumn(
                $setup->getTable('quote'),
                'webpos_cart_discount_name',
                array(
                    'type'      => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    'nullable'  => true,
                    'length'    => '255',
                    'comment'   => 'Web POS Discount Name'
                )
            );
        }

        // Abel edit: begin
        if (version_compare($context->getVersion(), '1.1.3', '<')) {
            $setup->getConnection()->addColumn(
                $setup->getTable('sales_order'),
                'serial_string',
                array(
                    'type'      => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    'nullable'  => true,
                    'length'    => null,
                    'default'   => null,
                    'comment'   => 'Serial Value'
                )
            );
            /* Add "barcode_value" into "sales_order_item" table */
            $setup->getConnection()->addColumn(
                $setup->getTable('sales_order_item'),
                'serial_string',
                array(
                    'type'      => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    'nullable'  => true,
                    'length'    => null,
                    'default'   => null,
                    'comment'   => 'Serial Value'
                )
            );
        }
        if (version_compare($context->getVersion(), '1.1.4', '<')) {
            $setup->getConnection()->addColumn(
                $setup->getTable('sales_order_item'),
                'serial_shipping',
                array(
                    'type'      => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    'nullable'  => true,
                    'length'    => null,
                    'default'   => null,
                    'comment'   => 'Serial Shipping'
                )
            );
            $setup->getConnection()->addColumn(
                $setup->getTable('sales_order_item'),
                'serial_refund',
                array(
                    'type'      => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    'nullable'  => true,
                    'length'    => null,
                    'default'   => null,
                    'comment'   => 'Serial Refund'
                )
            );
        }
        if (version_compare($context->getVersion(), '1.1.5', '<')) {
            $setup->getConnection()->addColumn(
                $setup->getTable('sales_creditmemo_item'),
                'serial_string',
                array(
                    'type'      => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    'nullable'  => true,
                    'length'    => null,
                    'default'   => null,
                    'comment'   => 'Serial string'
                )
            );
            $setup->getConnection()->addColumn(
                $setup->getTable('sales_shipment_item'),
                'serial_string',
                array(
                    'type'      => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    'nullable'  => true,
                    'length'    => null,
                    'default'   => null,
                    'comment'   => 'Serial string'
                )
            );
        }

        if (version_compare($context->getVersion(), '1.1.6', '<')) {
            $setup->getConnection()->addColumn(
                $setup->getTable('sales_shipment'),
                'serial_string',
                array(
                    'type'      => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    'nullable'  => true,
                    'length'    => null,
                    'default'   => null,
                    'comment'   => 'Serial string'
                )
            );
        }

        if (version_compare($context->getVersion(), '1.1.7', '<')) {
            $setup->getConnection()->addColumn(
                $setup->getTable('sales_shipment_grid'),
                'serial_string',
                array(
                    'type'      => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    'nullable'  => true,
                    'length'    => null,
                    'default'   => null,
                    'comment'   => 'Serial string'
                )
            );
        }
        // Abel edit: end
        
        $setup->endSetup();
    }
}
