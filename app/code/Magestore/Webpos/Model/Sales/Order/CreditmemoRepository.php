<?php

/**
 *  Copyright © 2016 Magestore. All rights reserved.
 *  See COPYING.txt for license details.
 *
 */
namespace Magestore\Webpos\Model\Sales\Order;

use Magento\Sales\Model\ResourceModel\Metadata as Metadata;;
use Magento\Sales\Api\Data\CreditmemoSearchResultInterfaceFactory as SearchResultFactory;

/**
 * Repository class for @see \Magento\Sales\Api\Data\CreditmemoInterface
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class CreditmemoRepository extends \Magento\Sales\Model\Order\CreditmemoRepository
    implements \Magestore\Webpos\Api\Sales\CreditmemoRepositoryInterface
{
    /**
     * @var Metadata
     */
    protected $metadata;

    /**
     * @var SearchResultFactory
     */
    protected $searchResultFactory = null;

    /**
     * @var \Magento\Sales\Controller\Adminhtml\Order\CreditmemoLoader
     */
    protected $creditmemoLoader;

    /**
     * @var \Magento\Sales\Api\CreditmemoManagementInterface
     */
    protected $creditmemoManagement;

    /**
     * @var \Magento\Sales\Model\Order\Email\Sender\CreditmemoSender
     */
    protected $creditmemoSender;

    /**
     * @var \Magento\Sales\Api\OrderRepositoryInterface
     */
    protected $orderRepository;

    /**
     * @var \Magestore\Webpos\Helper\Currency
     */
    protected $currencyHelper;

    /**
     * @var \Magento\Catalog\Model\ProductFactory
     */
    protected $productFactory;

    /**
     * CreditmemoRepository constructor.
     * @param Metadata $metadata
     * @param SearchResultFactory $searchResultFactory
     * @param \Magento\Sales\Controller\Adminhtml\Order\CreditmemoLoader $creditmemoLoader
     * @param \Magento\Sales\Api\CreditmemoManagementInterface $creditmemoManagement
     * @param \Magento\Sales\Model\Order\Email\Sender\CreditmemoSender $creditmemoSender
     * @param \Magento\Sales\Api\OrderRepositoryInterface $orderRepository
     */
    public function __construct(
        Metadata $metadata,
        SearchResultFactory $searchResultFactory,
        \Magento\Sales\Controller\Adminhtml\Order\CreditmemoLoader $creditmemoLoader,
        \Magento\Sales\Api\CreditmemoManagementInterface $creditmemoManagement,
        \Magento\Sales\Model\Order\Email\Sender\CreditmemoSender $creditmemoSender,
        \Magestore\Webpos\Api\Sales\OrderRepositoryInterface $orderRepository,
        \Magestore\Webpos\Helper\Currency $currencyHelper,
        \Magento\Catalog\Model\ProductFactory $productFactory
    ) {
        $this->metadata = $metadata;
        $this->searchResultFactory = $searchResultFactory;
        $this->creditmemoLoader = $creditmemoLoader;
        $this->creditmemoManagement = $creditmemoManagement;
        $this->creditmemoSender = $creditmemoSender;
        $this->orderRepository = $orderRepository;
        $this->currencyHelper = $currencyHelper;
        $this->productFactory = $productFactory;
    }

    /**
     * Performs persist operations for a specified credit memo.
     *
     * @param \Magento\Sales\Api\Data\CreditmemoInterface $entity The credit memo.
     * @return \Magestore\Webpos\Api\Data\Sales\OrderInterface Order interface.
     */
    public function saveCreditmemo(\Magento\Sales\Api\Data\CreditmemoInterface $entity){
        $data = $this->prepareCreditmemo($entity);
        $this->creditmemoLoader->setOrderId($data['order_id']);
        $this->creditmemoLoader->setCreditmemo($data['creditmemo']);
        $creditmemo = $this->creditmemoLoader->load();
        if ($creditmemo) {
            if (!$creditmemo->isValidGrandTotal()) {
                throw new \Magento\Framework\Exception\LocalizedException(
                    __('The credit memo\'s total must be positive.')
                );
            }
            if (!empty($data['creditmemo']['comment_text'])) {
                $creditmemo->addComment(
                    $data['creditmemo']['comment_text'],
                    isset($data['creditmemo']['comment_customer_notify']),
                    true
                );
                if(isset($data['creditmemo']['comment_text']))
                    $creditmemo->setCustomerNote($data['creditmemo']['comment_text']);
                if(isset($data['creditmemo']['comment_customer_notify']))
                    $creditmemo->setCustomerNoteNotify(isset($data['comment_customer_notify']));
            }
            // Abel edit: begin => save serial_string to memo item
            $newCreditmemo = $this->creditmemoManagement->refund($creditmemo, true, !empty($data['creditmemo']['send_email']));
            $newCreditmemoItems = $newCreditmemo->getAllItems();
            $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
            $orderItemFactory = $objectManager->get('Magento\Sales\Model\Order\ItemFactory');
            $listSerialNumberRefund = '';
            $listSerialNumberShip = '';
            foreach ($newCreditmemoItems as $newCreditmemoItem) {
                $orderItemId = $newCreditmemoItem->getData('order_item_id');
                if(isset($data['creditmemo']['items'][$orderItemId]['serial_string'])
                    && $data['creditmemo']['items'][$orderItemId]['serial_string'] != null) {
                    $newCreditmemoItem->setData('serial_string', $data['creditmemo']['items'][$orderItemId]['serial_string'])
                        ->save();
                    if(isset($data['creditmemo']['items'][$orderItemId]['back_to_stock'])
                        && $data['creditmemo']['items'][$orderItemId]['back_to_stock'] == 1) {
                        $listSerialNumberRefund .= ',' . $data['creditmemo']['items'][$orderItemId]['serial_string'];
                        $orderItemModel = $orderItemFactory->create()->load($orderItemId);
                        $listSerialNumberShip .= ',' . $orderItemModel->getData('serial_shipping');
                    }
                }
            }
            // create serial number transaction
            $listSerialNumberRefundTrans = '';
            foreach (explode(',', $listSerialNumberShip) as $serial_number_ship) {
                if ($serial_number_ship != '' && $serial_number_ship != null) {
                    if(strpos($listSerialNumberRefund, $serial_number_ship) !== false) {
                        $listSerialNumberRefundTrans .= ',' . $serial_number_ship;
                    }
                }
            }
            $transactionFactory = $objectManager->get('Magestore\SerialSuccess\Model\TransactionFactory');
            $serialFactory = $objectManager->get('Magestore\SerialSuccess\Model\ItemFactory');
            $refundId = $newCreditmemo->getId();
            $curDate = $objectManager->get('Magento\Framework\Stdlib\DateTime\DateTime')->gmtDate();
            foreach (explode(',', $listSerialNumberRefundTrans) as $serial_number_refund) {
                if ($serial_number_refund != '' && $serial_number_refund != null) {
                    $serialId = $serialFactory->create()->load($serial_number_refund, 'serial')->getId();
                    if($serialId) {
                        $transaction = $transactionFactory->create();
                        $transactionData = [
                            'serial_item_id' => $serialId,
                            'created_at' => $curDate,
                            'status' => 1,
                            'desc' => json_encode(['refund_id' => $refundId])
                        ];
                        $transaction->setData($transactionData)->save();
                    }
                }
            }
            // Abel edit: end

            if (!empty($data['creditmemo']['send_email'])) {
                $this->creditmemoSender->send($creditmemo);
            }
        }
        return $this->orderRepository->get($data['order_id']);
    }

    /**
     * @param \Magento\Sales\Api\Data\CreditmemoInterface $entity
     * @return array
     */
    protected function prepareCreditmemo(\Magento\Sales\Api\Data\CreditmemoInterface $entity){
        $data = [];
        $items = $entity->getItems();
        $orderId = $entity->getOrderId();
        if(count($items>0) && $orderId){
            $data['order_id'] = $orderId;
            $creditmemo = [];
            $objectManager = \Magento\Framework\App\ObjectManager::getInstance(); // Abel edit
            foreach ($items as $item){
                $creditmemo['items'][$item->getOrderItemId()]['qty'] = $item->getQty();
                // Abel edit: begin => save serial_refund to order item
                $additionalData = json_decode($item->getAdditionalData(), true);
                if(isset($additionalData['back_to_stock']) && $additionalData['back_to_stock'] == 'back_to_stock') {
                    $creditmemo['items'][$item->getOrderItemId()]['back_to_stock'] = 1;
                    if(isset($additionalData['serial_string'])) {
                        $serial_string = explode(',', $additionalData['serial_string']);
                        foreach ($serial_string as $sn) {
                            if($sn != '' && $sn != null) {
                                $snModel = $objectManager->get('Magestore\SerialSuccess\Model\ItemFactory')->create();
                                $snModel = $snModel->load($sn, 'serial');
                                $snModel->setData('status', 1);
                                $snModel->save();
                            }
                        }
                    }
                }
                if(isset($additionalData['serial_string'])) {
                    $orderItem = $objectManager->get('Magento\Sales\Model\Order\ItemFactory')->create();
                    $orderItem = $orderItem->load($item->getOrderItemId());
                    $sn = $orderItem->getData('serial_refund');
                    $sn .= ','.$additionalData['serial_string'];
                    $orderItem->setData('serial_refund', $sn)->save();

                    $creditmemo['items'][$item->getOrderItemId()]['serial_string'] = $additionalData['serial_string'];
                }
                // Abel edit: end
            }
            $creditmemo['send_email'] = $entity->getEmailSent();
            $comments = $entity->getComments();
            if(count($comments) && $comment = $comments[0]){
                $creditmemo['comment_text'] = $comment->getComment();
                if($creditmemo['send_email'])
                    $creditmemo['comment_customer_notify'] = 1;
            }
            $baseCurrencyCode = $entity->getBaseCurrencyCode();
            $storeCurrencyCode = $entity->getStoreCurrencyCode();
            $creditmemo['shipping_amount'] = $this->currencyHelper->currencyConvert($entity->getShippingAmount(), $storeCurrencyCode, $baseCurrencyCode);
            $creditmemo['adjustment_positive'] = $this->currencyHelper->currencyConvert($entity->getAdjustmentPositive(), $storeCurrencyCode, $baseCurrencyCode);
            $creditmemo['adjustment_negative'] = $this->currencyHelper->currencyConvert($entity->getAdjustmentNegative(), $storeCurrencyCode, $baseCurrencyCode);
            $data['creditmemo'] = $creditmemo;
            return $data;
        }
    }
}
