<?php

/**
 *  Copyright © 2016 Magestore. All rights reserved.
 *  See COPYING.txt for license details.
 *
 */
namespace Magestore\Webpos\Model\Sales\Order;

use Magento\Sales\Model\ResourceModel\Metadata as Metadata;
use Magento\Sales\Api\Data\ShipmentSearchResultInterfaceFactory as SearchResultFactory;

class ShipmentRepository extends \Magento\Sales\Model\Order\ShipmentRepository
    implements \Magestore\Webpos\Api\Sales\ShipmentRepositoryInterface
{
    /**
     * @var \Magento\Shipping\Controller\Adminhtml\Order\ShipmentLoader
     */
    protected $shipmentLoader;/**
 * 
     * @var \Magento\Framework\DB\Transaction
     */
    protected $dbTransaction;

    /**
     * @var ShipmentSender
     */
    protected $shipmentSender;

    /**
     * @var \Magento\Sales\Api\OrderRepositoryInterface
     */
    protected $orderRepository;

    /**
     * ShipmentRepository constructor.
     * @param Metadata $metadata
     * @param SearchResultFactory $searchResultFactory
     * @param \Magento\Shipping\Controller\Adminhtml\Order\ShipmentLoader $shipmentLoader
     * @param \Magento\Framework\DB\TransactionFactory $dbTransaction
     * @param \Magento\Sales\Model\Order\Email\Sender\ShipmentSender $shipmentSender
     * @param \Magento\Sales\Api\OrderRepositoryInterface $orderRepository
     */
    public function __construct(
        Metadata $metadata, 
        SearchResultFactory $searchResultFactory,
        \Magento\Shipping\Controller\Adminhtml\Order\ShipmentLoader $shipmentLoader,
        \Magento\Framework\DB\TransactionFactory $dbTransaction,
        \Magento\Sales\Model\Order\Email\Sender\ShipmentSender $shipmentSender,
        \Magestore\Webpos\Api\Sales\OrderRepositoryInterface $orderRepository
    ){
        parent::__construct($metadata, $searchResultFactory);
        $this->shipmentLoader = $shipmentLoader;
        $this->dbTransaction = $dbTransaction;
        $this->shipmentSender = $shipmentSender;
        $this->orderRepository = $orderRepository;
    }


    /**
     * Performs persist operations for a specified shipment.
     *
     * @param \Magento\Sales\Api\Data\ShipmentInterface $entity
     * @return \Magestore\Webpos\Api\Data\Sales\OrderInterface
     * @throws CouldNotSaveException
     */
    public function saveShipment(\Magento\Sales\Api\Data\ShipmentInterface $entity){
        $data = $this->_prepareShipment($entity);
        $this->shipmentLoader->setOrderId($data['order_id']);
        $this->shipmentLoader->setShipment($data['shipment']);
        $this->shipmentLoader->setTracking($data['tracking']);
        $shipment = $this->shipmentLoader->load();
        if (!empty($data['shipment']['comment_text'])) {
            $shipment->addComment(
                $data['shipment']['comment_text'],
                isset($data['shipment']['comment_customer_notify']),
                isset($data['shipment']['is_visible_on_front'])
            );

            $shipment->setCustomerNote($data['shipment']['comment_text']);
            $shipment->setCustomerNoteNotify(isset($data['shipment']['comment_customer_notify']));
        }
        $shipment->register();
        $shipment->getOrder()->setCustomerNoteNotify(!empty($data['shipment']['send_email']));
        // Abel edit: begin => save serial_string to shipment item
        $newShipment = $this->_saveShipment($shipment);
        $newShipmentItems = $newShipment->getAllItems();
        foreach ($newShipmentItems as $newShipmentItem) {
            $orderItemId = $newShipmentItem->getData('order_item_id');
            if(isset($data['shipment']['items']['serial_string'][$orderItemId])
                && $data['shipment']['items']['serial_string'][$orderItemId] != null) {
                $newShipmentItem->setData('serial_string', $data['shipment']['items']['serial_string'][$orderItemId])
                    ->save();
            }
        }
        // create transaction for serial number
        $shippingSerialString = '';
        foreach ($data['shipment']['items']['serial_string'] as $str) {
            $shippingSerialString .= ',' . $str;
        }
        $this->createSerialNumberTransaction($shippingSerialString, $newShipment->getId());

        // save serial_string for shipment
        $curShipmentSerial = $shipment->getData('serial_string');
        $shipment->setData('serial_string', $curShipmentSerial . ',' . $shippingSerialString)->save();
        // Abel edit: end
        if (!empty($data['shipment']['send_email'])) {
            $this->shipmentSender->send($shipment);
        }
        return $this->orderRepository->get($data['order_id']);
    }

    // Abel edit: begin => create transaction for serial number
    public function createSerialNumberTransaction($serial_string, $shippingId) {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $transactionFactory = $objectManager->get('Magestore\SerialSuccess\Model\TransactionFactory');
        $serialFactory = $objectManager->get('Magestore\SerialSuccess\Model\ItemFactory');
        $curDate = $objectManager->get('Magento\Framework\Stdlib\DateTime\DateTime')->gmtDate();
        foreach (explode(',', $serial_string) as $sn) {
            if($sn != '' && $sn != null) {
                $serialId = $serialFactory->create()->load($sn, 'serial')->getId();
                if($serialId) {
                    $transaction = $transactionFactory->create();
                    $transactionData = [
                        'serial_item_id' => $serialId,
                        'created_at' => $curDate,
                        'status' => 2,
                        'desc' => json_encode(['shipping_id' => $shippingId])
                    ];
                    $transaction->setData($transactionData)->save();
                }
            }
        }
    }
    // Abel edit: end

    /**
     * Save shipment and order in one transaction
     *
     * @param \Magento\Sales\Model\Order\Shipment $shipment
     * @return \Magento\Sales\Model\Order\Shipment
     */
    protected function _saveShipment($shipment)
    {
        $shipment->getOrder()->setIsInProcess(true);
        $this->dbTransaction->create()->addObject(
            $shipment
        )->addObject(
            $shipment->getOrder()
        )->save();

        return $shipment;
    }

    protected function _prepareShipment(\Magento\Sales\Api\Data\ShipmentInterface $entity){
        $data = [];
        $items = $entity->getItems();
        $orderId = $entity->getOrderId();
        if(count($items>0) && $orderId){
            $data['order_id'] = $orderId;
            $tracks = $entity->getTracks();
            $data['tracking'] = null;
            if(count($tracks) && $track = $tracks[0]){
                $trackData = [];
                $trackData['carrier_code'] = 'custom';
                $trackData['number'] = $track->getTrackNumber();
                $data['tracking'][] = $trackData;
            }
            $shipment = [];
            $objectManager = \Magento\Framework\App\ObjectManager::getInstance(); // Abel edit
            foreach ($items as $item){
                $shipment['items'][$item->getOrderItemId()] = $item->getQty();
                // Abel edit: begin => save serial_shipping to order item
                $additionalData = json_decode($item->getAdditionalData(), true);
                if(isset($additionalData['serial_string'])) {
                    $orderItem = $objectManager->get('Magento\Sales\Model\Order\ItemFactory')->create();
                    $orderItem = $orderItem->load($item->getOrderItemId());
                    $sn = $orderItem->getData('serial_shipping');
                    $sn .= ','.$additionalData['serial_string'];
                    $orderItem->setData('serial_shipping', $sn)->save();

                    $shipment['items']['serial_string'][$item->getOrderItemId()] = $additionalData['serial_string'];
                }
                // Abel edit: end
            }
            $shipment['send_email'] = $entity->getEmailSent();
            $comments = $entity->getComments();
            if(count($comments) && $comment = $comments[0]){
                $shipment['comment_text'] = $comment->getComment();
                if($shipment['send_email'])
                    $shipment['comment_customer_notify'] = 1;
            }
            $data['shipment'] = $shipment;
            return $data;
        }
        return null;
    }
}