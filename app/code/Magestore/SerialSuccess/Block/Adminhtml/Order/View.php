<?php

namespace Magestore\SerialSuccess\Block\Adminhtml\Order;

class View extends \Magento\Backend\Block\Widget\Form\Generic
{

    protected $_template = 'Magestore_SerialSuccess::order/add-serial-modal.phtml';

}