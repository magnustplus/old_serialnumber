<?php
namespace Magestore\SerialSuccess\Block\Adminhtml\Product\Edit\Tab;

class Serial extends \Magento\Backend\Block\Template {
    protected $_template = 'Magestore_SerialSuccess::product/edit/tab/container.phtml';
    protected $blockGrid;

    /**
     * Retrieve instance of grid block
     *
     * @return \Magento\Framework\View\Element\BlockInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getBlockGrid()
    {
        if (null === $this->blockGrid) {
            $this->blockGrid = $this->getLayout()->createBlock(
                'Magestore\SerialSuccess\Block\Adminhtml\Product\Edit\Tab\Serial\Grid',
                'product.serial.grid'
            );
        }
        return $this->blockGrid;
    }

    /**
     * Return HTML of grid block
     *
     * @return string
     */
    public function getGridHtml()
    {
        return $this->getBlockGrid()->toHtml();
    }
}