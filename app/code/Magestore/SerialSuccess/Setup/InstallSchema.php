<?php
namespace Magestore\SerialSuccess\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

class InstallSchema implements InstallSchemaInterface {
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context) {
        $setup->startSetup();

        $setup->getConnection()->dropTable($setup->getTable('os_serial_items'));
        $setup->getConnection()->dropTable($setup->getTable('os_serial_transactions'));

        $table = $setup->getConnection()->newTable($setup->getTable('os_serial_items'))
                       ->addColumn(
                           'serial_item_id',
                           \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                           null,
                           [
                               'identity' => true,
                               'unsigned' => true,
                               'nullable' => false,
                               'primary' => true,
                           ],
                           'Serial item id'
                       ) ->addColumn(
                            'product_id',
                            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                            null,
                            array(
                                'unsigned' => true,
                                'nullable' => false
                            ),
                            'Product id'
                        )->addColumn(
                            'serial',
                            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                            50,
                            array(),
                            'Serial content'
                        )->addColumn(
                            'created_at',
                            \Magento\Framework\DB\Ddl\Table::TYPE_DATETIME,
                            null,
                            array(),
                            'created_at'
                        )->addColumn(
                            'status',
                            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                            50,
                            array(),
                            'status'
                        );
        $tranTable = $setup->getConnection()->newTable($setup->getTable('os_serial_transactions'))
                       ->addColumn(
                           'serial_trans_id',
                           \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                           null,
                           [
                               'identity' => true,
                               'unsigned' => true,
                               'nullable' => false,
                               'primary' => true,
                           ],
                           'Transaction id'
                        )->addColumn(
                            'serial_item_id',
                            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                            10,
                            array(),
                            'Serial id'
                        )->addColumn(
                            'created_at',
                            \Magento\Framework\DB\Ddl\Table::TYPE_DATETIME,
                            null,
                            array(),
                            'created_at'
                        )->addColumn(
                            'status',
                            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                            50,
                            array(),
                            'Status'
                        )->addColumn(
                            'desc',
                            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                            250,
                            array(),
                            'Description'
                        );
        $setup->getConnection()->createTable($table);
        $setup->getConnection()->createTable($tranTable);
        $setup->endSetup();
    }
}