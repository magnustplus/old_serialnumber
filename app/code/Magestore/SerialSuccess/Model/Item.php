<?php
namespace Magestore\SerialSuccess\Model;

use Magento\Framework\Model\Context;

class Item extends \Magento\Framework\Model\AbstractModel {
    public function __construct(
        Context $context,
        \Magento\Framework\Registry $registry,
        \Magestore\SerialSuccess\Model\ResourceModel\Item $resource = null,
        \Magestore\SerialSuccess\Model\ResourceModel\Item\Collection $resourceCollection)
    {
        parent::__construct($context, $registry, $resource, $resourceCollection);
    }
}