<?php
namespace Magestore\SerialSuccess\Model\ResourceModel;

class Transaction extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb {
    public function _construct() {
        $this->_init('os_serial_transactions', 'serial_trans_id');
    }
}