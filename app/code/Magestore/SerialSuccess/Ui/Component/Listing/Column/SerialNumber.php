<?php
namespace Magestore\SerialSuccess\Ui\Component\Listing\Column;

use Magento\Framework\Escaper;
use Magento\Framework\View\Element\UiComponent\ContextInterface;
use Magento\Framework\View\Element\UiComponentFactory;

class SerialNumber extends \Magento\Ui\Component\Listing\Columns\Column {

    /**
     * Escaper
     *
     * @var \Magento\Framework\Escaper
     */
    protected $escaper;

    protected $serialFactory;

    /**
     * SerialNumber constructor.
     * @param ContextInterface $context
     * @param UiComponentFactory $uiComponentFactory
     * @param Escaper $escaper
     * @param \Magestore\SerialSuccess\Model\ItemFactory $serialFactory
     * @param array $components
     * @param array $data
     */
    public function __construct(
        ContextInterface $context,
        UiComponentFactory $uiComponentFactory,
        Escaper $escaper,
        \Magestore\SerialSuccess\Model\ItemFactory $serialFactory,
        array $components = [],
        array $data = []
    ) {
        $this->serialFactory = $serialFactory;
        $this->escaper = $escaper;
        parent::__construct($context, $uiComponentFactory, $components, $data);
    }

    /**
     * Prepare Data Source
     *
     * @param array $dataSource
     * @return array
     */
    public function prepareDataSource(array $dataSource)
    {
        if (isset($dataSource['data']['items'])) {
            foreach ($dataSource['data']['items'] as & $item) {
                $serial = $this->serialFactory->create()->load((int)$item[$this->getData('name')]);
                $item[$this->getData('name')] = $serial->getData('serial');
            }
        }

        return $dataSource;
    }
}