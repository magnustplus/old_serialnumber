<?php
// Abel edit: add column serial string to order item table
namespace Magestore\SerialSuccess\Plugin\Order\View\Items\Renderer;

use Magento\Sales\Block\Adminhtml\Order\View\Items\Renderer\DefaultRenderer as OldDefaultRenderer;

class DefaultRenderer extends OldDefaultRenderer {
    public function _beforeToHtml()
    {
        parent::_beforeToHtml();
        $this->setTemplate('Magestore_SerialSuccess::order/view/items/renderer/default.phtml');
        return $this;
    }
}