/*
 * Copyright © 2016 Magestore. All rights reserved.
 * See COPYING.txt for license details.
 */

define([
    'jquery',
    'Magestore_SerialSuccess/js/form/elements/import-button'
], function ($, Component, ko) {
    'use strict';

    var self = this;
    return Component.extend({
        defaults: {
            links: {
                value: '${ $.provider }:${ $.dataScope }',
            },
            serialData: ''
        },

        /** @inheritdoc */
        initObservable: function () {
            return this._super()
                .observe([
                    'visible',
                    'disabled',
                    'title',
                    'serialData'
                ]);
        },

        handleOnclick: function (data, event) {
            if (event.target.name !== '') {
                window.removeAllSerial();
                var productId = event.target.name;
                $('#product-id').html(productId);
                if (typeof window.serials[productId] === 'undefined')
                    window.serials[productId] = [];
                window.serials[productId].forEach(function (e) {
                    window.addSerial(e);
                });
                window.titleUpdater = this.title;
                $('#add-serial-modal').modal('openModal');
            }
        }
    });
});
