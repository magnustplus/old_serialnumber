<?php
namespace Magestore\SerialSuccess\Controller\Adminhtml\Serials;

use Magento\Backend\App\Action;
use Magestore\SerialSuccess\Controller\Adminhtml\AbstractAction;

class Remove extends AbstractAction {

    public function execute() {
        $serialId = $this->getRequest()->getParam('id');
        if ($serialId) {
            $serialItem = $this->_itemFactory->create()->load($serialId);
            $productId = $serialItem->getData('product_id');
            $serialItem->delete();

            $transactions = $this->_transactionFactory->create()
                ->getCollection()
                ->addFieldToFilter('serial_item_id', ['eq' => $serialId])
                ->getColumnValues('serial_trans_id');
            foreach ($transactions as $transaction) {
                $this->_transactionFactory->create()->load($transaction)->delete();
            }
            $this->messageManager->addSuccessMessage(
                __('Serial has been deleted successfully')
            );
        } else
            $this->messageManager->addErrorMessage(
                __('Error! Serial is no longer exist')
            );

        return $this->resultRedirectFactory->create()
                ->setPath('catalog/product/edit', ['id' => $productId]);
    }
}