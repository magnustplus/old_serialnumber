<?php

namespace Magestore\SerialSuccess\Controller\Adminhtml\Serials\Product;

use Magento\Backend\App\Action;

class Grid extends \Magestore\SerialSuccess\Controller\Adminhtml\AbstractAction {
    protected $_stockGrid = 'Magestore\SerialSuccess\Block\Adminhtml\Product\Edit\Tab\Serial\Grid';
    protected $_stockGridName = 'product.serials.grid';

    /**
     * @var \Magento\Framework\Controller\Result\RawFactory
     */
    protected $resultRawFactory;

    /**
     * @var \Magento\Framework\View\LayoutFactory
     */
    protected $layoutFactory;


    public function __construct (
        Action\Context $context,
        \Magento\Framework\Controller\Result\RawFactory $resultRawFactory,
        \Magento\Framework\View\LayoutFactory $layoutFactory
    ) {
        $this->resultRawFactory = $resultRawFactory;
        $this->layoutFactory = $layoutFactory;
        parent::__construct($context);
    }

    /**
     * Grid Action
     * Display list of products related to current category
     *
     * @return \Magento\Framework\Controller\Result\Raw
     */
    public function execute()
    {
        /** @var \Magento\Framework\Controller\Result\Raw $resultRaw */
        $resultRaw = $this->resultRawFactory->create();
        return $resultRaw->setContents(
            $this->layoutFactory->create()->createBlock(
                $this->_stockGrid,
                $this->_stockGridName
            )->toHtml()
        );
    }
}