<?php
namespace Magestore\SerialSuccess\Controller\Adminhtml\Serials;

use Magento\Backend\App\Action;
use Magento\Framework\App\ResponseInterface;
use Magestore\SerialSuccess\Controller\Adminhtml\AbstractAction;

class InlineEdit extends AbstractAction {
    /**
     * @var \Magento\Framework\Controller\Result\JsonFactory
     */
    protected $resultJsonFactory;

    protected $_error = 0;

    protected $_messages = [];

    public function __construct (
        Action\Context $context,
        \Magestore\SerialSuccess\Model\ItemFactory $itemFactory,
        \Magestore\SerialSuccess\Model\TransactionFactory $transactionFactory,
        \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory
    ) {
        $this->resultJsonFactory = $resultJsonFactory;
        parent::__construct($context, $itemFactory, $transactionFactory);
    }

    /**
     * Dispatch request
     *
     * @return \Magento\Framework\Controller\ResultInterface|ResponseInterface
     * @throws \Magento\Framework\Exception\NotFoundException
     */
    public function execute() {
        /** @var \Magento\Framework\Controller\Result\Json $resultJson */
        $resultJson = $this->resultJsonFactory->create();

        $postItems = $this->getRequest()->getParam('items', []);
        if (!($this->getRequest()->getParam('isAjax') && count($postItems))) {
            return $resultJson->setData([
                'messages' => [__('Please correct the data sent.')],
                'error' => true,
            ]);
        }

        foreach ($postItems as $key => $value) {
            $this->_itemFactory->create()
                ->load($key)
                ->setData($value)
                ->save();
        }

        return $resultJson->setData([
            'messages' => [],
            'error' => 0
        ]);
    }
}