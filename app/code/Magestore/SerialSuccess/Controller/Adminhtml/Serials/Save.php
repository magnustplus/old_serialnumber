<?php
namespace Magestore\SerialSuccess\Controller\Adminhtml\Serials;

use Magento\Backend\App\Action;

class Save extends \Magestore\SerialSuccess\Controller\Adminhtml\AbstractAction {

    /** @var \Magestore\SerialSuccess\Model\ResourceModel\Item\CollectionFactory  */
    protected $_itemCollectionFactory;

    /** @var \Magento\CatalogInventory\Model\Stock\StockItemRepository  */
    protected $_stockItemRepository;

    public function __construct (
        Action\Context $context,
        \Magestore\SerialSuccess\Model\ItemFactory $itemFactory,
        \Magestore\SerialSuccess\Model\TransactionFactory $transactionFactory,
        \Magestore\SerialSuccess\Model\ResourceModel\Item\CollectionFactory $collectionFactory,
        \Magento\CatalogInventory\Model\Stock\StockItemRepository $stockItemRepository
    ) {
        $this->_itemCollectionFactory = $collectionFactory;
        $this->_stockItemRepository = $stockItemRepository;
        parent::__construct($context, $itemFactory, $transactionFactory);
    }

    public function execute() {
        $data = $this->getRequest()->getParams();
        if ($data) {
            $serials = json_decode($data['data']);
            $numOfSerial = sizeof($serials);

            $productId = $data['id'];

            $itemCollection = $this->_itemCollectionFactory->create()
                ->addFieldToFilter(
                    'product_id',
                    ['eq' => $productId]
                );

            $productQty = $this->_stockItemRepository->get($productId)->getQty();
            if ($productQty == null) {
                echo __('Your serials cannot be added due to a stock quantity problem, please check product qty and try again');
                return;
            }
            $currentSerialQty = $itemCollection->count();

            $availableSlot = $productQty - $currentSerialQty;
            if ($availableSlot < 0) $availableSlot = 0;
            $serials = array_slice(
                $serials,
                0,
                min($availableSlot, sizeof($serials))
            );

            $diff = array_diff($serials, $itemCollection->getColumnValues('serial'));
            foreach ($diff as $add) {
                $item = $this->_itemFactory->create();
                $item->setData('product_id', $productId);
                $item->setData('serial', $add);
                $item->setData('created_at', time());
                $item->setData('status', 1);
                $item->save();

                $trans = $this->_transactionFactory->create();
                $trans->setData('serial_item_id', $item->getId());
                $trans->setData('status', 1);
                $trans->setData('created_at', time());
                $trans->setData('desc', 'created_manually');
                $trans->save();
            }

            $ignored = $numOfSerial - sizeof($diff);
            if ($ignored > 0)
                echo __($ignored . ' serial cannot be added as they have already been exist in this product');
        }
    }
}