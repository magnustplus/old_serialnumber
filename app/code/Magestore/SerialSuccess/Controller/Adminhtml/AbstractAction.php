<?php
namespace Magestore\SerialSuccess\Controller\Adminhtml;

use Magento\Backend\App\Action;

class AbstractAction extends \Magento\Backend\App\Action {

    /** @var \Magestore\SerialSuccess\Model\ItemFactory  */
    protected $_itemFactory;

    /** @var \Magestore\SerialSuccess\Model\TransactionFactory  */
    protected $_transactionFactory;

    public function __construct (
        Action\Context $context,
        \Magestore\SerialSuccess\Model\ItemFactory $itemFactory,
        \Magestore\SerialSuccess\Model\TransactionFactory $transactionFactory
    ) {
        $this->_itemFactory = $itemFactory;
        $this->_transactionFactory = $transactionFactory;
        parent::__construct($context);
    }

    public function execute() {
    }
}