<?php
namespace Magestore\SerialSuccess\Controller\Adminhtml\Transaction;

use Magento\Backend\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;
use Magento\Backend\App\Action;

class Index extends Action {
    const ADMIN_RESOURCE = 'Magestore_SerialSuccess::transaction';

    /**
     * @var PageFactory
     */
    protected $resultPageFactory;

    /**
     * @param Context $context
     * @param PageFactory $resultPageFactory
     */
    public function __construct(
        Context $context,
        PageFactory $resultPageFactory
    ) {
        parent::__construct($context);
        $this->resultPageFactory = $resultPageFactory;
    }

    /**
     * Index action
     *
     * @return \Magento\Backend\Model\View\Result\Page
     */
    public function execute()
    {
        /** @var \Magento\Backend\Model\View\Result\Page $resultPage */
        $resultPage = $this->resultPageFactory->create();
        $resultPage->setActiveMenu('Magestore_SerialSuccess::manage_transaction');
        $resultPage->addBreadcrumb(__('Serial Number'), __('Serial Number'));
        $resultPage->addBreadcrumb(__('Transaction'), __('Transaction'));
        $resultPage->getConfig()->getTitle()->prepend(__('Transaction'));

        return $resultPage;
    }
}