<?php
namespace Magestore\SerialSuccess\Controller\Adminhtml\Order;

class Saveserialnumber extends \Magestore\SerialSuccess\Controller\Adminhtml\AbstractAction {
    public function execute()
    {
        $orderItemId = $this->getRequest()->getParam('itemId');
        $listSerial = $this->getRequest()->getParam('data');

        $orderItem = $this->_objectManager->get('Magento\Sales\Model\Order\ItemFactory')
            ->create()->load($orderItemId);
        if($orderItem->getId()) {
            $productId = '';
            if($orderItem->getData('product_type') == 'simple') {
                $productId = $orderItem->getData('product_id');
            } elseif ($orderItem->getData('product_type') == 'configurable') {
                $childItem = $this->_objectManager->get('Magento\Sales\Model\Order\ItemFactory')
                    ->create()->load($orderItem->getId(), 'parent_item_id');
                $productId = $childItem->getData('product_id');
            }
            $curSerialNumber = $this->_itemFactory->create()->getCollection()
                ->addFieldToFilter('status', 1)
                ->addFieldToFilter('product_id', $productId)
                ->getColumnValues('serial');

            $correctSerial = array_intersect($curSerialNumber, $listSerial);
            $serialString = '';
            foreach ($correctSerial as $serial) {
                $serialItem = $this->_itemFactory->create()
                    ->load($serial, 'serial');
                if($serialItem->getId()) {
                    $serialItem->setData('status', 2)->save();
                    $serialString .= ',' . $serial;
                }
            }

            $curSerialString = $orderItem->getData('serial_string');
            $orderItem->setData('serial_string', $curSerialString.','.$serialString)->save();

            // Save serial_string to sales_order
            $orderModel = $orderItem->getOrder();
            $curOrderSerialString = $orderModel->getData('serial_string');
            $orderModel->setData('serial_string', $curOrderSerialString .','. $serialString)->save();
        }
    }
}