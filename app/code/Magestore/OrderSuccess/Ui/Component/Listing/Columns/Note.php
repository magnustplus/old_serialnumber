<?php
/**
 * Copyright © 2016 Magestore. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Magestore\OrderSuccess\Ui\Component\Listing\Columns;

use Magento\Framework\View\Element\UiComponentFactory;
use Magento\Framework\View\Element\UiComponent\ContextInterface;

/**
 * Class Note
 * @package Magestore\OrderSuccess\Ui\Component\Listing\Columns
 */
class Note extends \Magento\Ui\Component\Listing\Columns\Column
{
    /**
     * string
     */
    const NAME = 'note';

    /**
     * string
     */
    const ALT_FIELD = 'name';

    /**
     * string
     */
    protected $storeManager;

    /**
     * Note constructor.
     * @param ContextInterface $context
     * @param UiComponentFactory $uiComponentFactory
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param array $components
     * @param array $data
     */
    public function __construct(
        ContextInterface $context,
        UiComponentFactory $uiComponentFactory,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        array $components = [],
        array $data = []
    ) {
        parent::__construct($context, $uiComponentFactory, $components, $data);
        $this->storeManager = $storeManager;
    }

    /**
     * Prepare Data Source
     *
     * @param array $dataSource
     * @return array
     */
    public function prepareDataSource(array $dataSource)
    {
        if (isset($dataSource['data']['items'])) {
            $fieldName = $this->getData('name');
            foreach ($dataSource['data']['items'] as & $item) {
                $noteIcon = $this->getAlt($item) ? 'magestore/order/note_available.png' : 'magestore/order/note.png';
                $noteIconPath = $this->storeManager->getStore()
                        ->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA).$noteIcon;
                $item[$fieldName . '_src'] = $noteIconPath;
                $item[$fieldName . '_orig_src'] = $this->getAlt($item) ?: '';
                $item[$fieldName . '_content'] = $this->getAlt($item) ?: '';
            }
        }

        return $dataSource;
    }

    /**
     * @param array $row
     *
     * @return null|string
     */
    protected function getAlt($row)
    {
        $altField = $this->getData('config/altField') ?: self::ALT_FIELD;
        return isset($row[$altField]) ? $row[$altField] : null;
    }
}
