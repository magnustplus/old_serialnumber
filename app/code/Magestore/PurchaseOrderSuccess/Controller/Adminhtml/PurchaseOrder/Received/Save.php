<?php
/**
 * Copyright © 2016 Magestore. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Magestore\PurchaseOrderSuccess\Controller\Adminhtml\PurchaseOrder\Received;

/**
 * Class Save
 * @package Magestore\PurchaseOrderSuccess\Controller\Adminhtml\Quotation
 */
class Save extends \Magestore\PurchaseOrderSuccess\Controller\Adminhtml\AbstractAction
{
    /**
     * Authorization level of a basic admin session
     */
    const ADMIN_RESOURCE = 'Magestore_PurchaseOrderSuccess::received_purchase_order';

    /**
     * @var \Magestore\PurchaseOrderSuccess\Service\PurchaseOrder\PurchaseOrderService
     */
    protected $purchaseOrderService;

    /**
     * @var \Magestore\PurchaseOrderSuccess\Service\PurchaseOrder\Item\Received\ReceivedService
     */
    protected $receivedService;

    /**
     * @var \Magento\Framework\Stdlib\DateTime\TimezoneInterface
     */
    protected $timezone;

    /**
     * @var \Magento\Framework\ObjectManagerInterface
     */
    protected $_objectManager;

    public function __construct(
        \Magestore\PurchaseOrderSuccess\Controller\Adminhtml\Context $context,
        \Magestore\PurchaseOrderSuccess\Service\PurchaseOrder\PurchaseOrderService $purchaseOrderService,
        \Magestore\PurchaseOrderSuccess\Service\PurchaseOrder\Item\Received\ReceivedService $receivedService,
        \Magento\Framework\Stdlib\DateTime\TimezoneInterface $timezone,
        \Magento\Framework\ObjectManagerInterface $objectManager
    ){
        parent::__construct($context);
        $this->purchaseOrderService = $purchaseOrderService;
        $this->receivedService = $receivedService;
        $this->timezone = $timezone;
        $this->_objectManager = $objectManager;
    }

    /**
     * Quotation grid
     *
     * @return \Magento\Backend\Model\View\Result\Page
     */
    public function execute()
    {
        $params = $this->getRequest()->getParams();

        $resultRedirect = $this->resultRedirectFactory->create();
        if(!isset($params['purchase_order_id'])){
            $this->messageManager->addErrorMessage(__('Please select a purchase order to received product'));
            return $resultRedirect->setPath('*/purchaseOrder/');
        }

        if(!isset($params['dynamic_grid'])){
            $this->messageManager->addErrorMessage(__('Please receive at least one product.'));
            return $resultRedirect->setPath('*/purchaseOrder/view',['id'=>$params['purchase_order_id']]);
        }
        $receivedData = $this->receivedService->processReceivedData($params['dynamic_grid']);
        try {
            $user = $this->_auth->getUser();
            $this->purchaseOrderService->receiveProducts(
                $params['purchase_order_id'], $receivedData, $params['received_at'], $user->getUserName()
            );
            $this->messageManager->addSuccessMessage(__('Receive products successfully.'));
        } catch (\Exception $e) {
            $this->messageManager->addErrorMessage($e->getMessage());
        }

        /*
         * <customization
         *      author="Mino"
         *      time="2017/05/04"\
         *      content="Add integration with SerialSuccess" >
         */
        $serialSuccessEnabled = $this->_objectManager->create('Magento\Framework\Module\Manager')
            ->isEnabled('Magestore_InventorySuccess');
        if ($serialSuccessEnabled) {
            /** @var \Magestore\SerialSuccess\Model\ItemFactory $serialItemFactory */
            /** @var \Magestore\SerialSuccess\Model\TransactionFactory $serialTransFactory */
            /** @var Magestore\SerialSuccess\Model\ResourceModel\Item\CollectionFactory $collectionFactory */
            $serialItemFactory = $this->_objectManager->create('Magestore\SerialSuccess\Model\ItemFactory');
            $serialTransFactory = $this->_objectManager->create('Magestore\SerialSuccess\Model\TransactionFactory');
            $collectionFactory = $this->_objectManager->create('Magestore\SerialSuccess\Model\ResourceModel\Item\CollectionFactory');
            $duplicate = 0;

            $desc = json_encode(["from_po" => $params['purchase_order_id']]);
            foreach ($params['dynamic_grid'] as &$p) {
                $deleteQty = sizeof($p['serial-data']) - $p['received_qty'];
                if ($deleteQty > 0)
                    for ($i = 0; $i < $deleteQty; $i++)
                        array_pop($p['serial-data']);

                $serials = $collectionFactory->create()
                    ->addFieldToSelect('serial')
                    ->addFieldToFilter('product_id', ['eq' => $p['id']])
                    ->getColumnValues('serial');

                $serials = array_diff($p['serial-data'], $serials);

                foreach ($serials as $serial) {
                    $item = $serialItemFactory->create();
                    $item->setData('product_id', $p['id']);
                    $item->setData('serial', $serial);
                    $item->setData('created_at', time());
                    $item->setData('status', 1);
                    $item->save();

                    $trans = $serialTransFactory->create();
                    $trans->setData('serial_item_id', $item->getId());
                    $trans->setData('status', 1);
                    $trans->setData('created_at', time());
                    $trans->setData('desc', $desc);
                    $trans->save();
                }
            }
            if ($duplicate != 0)
                $this->messageManager->addNoticeMessage(
                    __('Warning! We have found ' . $duplicate . ' duplicate serials and ignored them, you can manually add them later')
                );
        }
        // </customization>

        return $resultRedirect->setPath('*/purchaseOrder/view', ['id' => $params['purchase_order_id']]);
    }

}