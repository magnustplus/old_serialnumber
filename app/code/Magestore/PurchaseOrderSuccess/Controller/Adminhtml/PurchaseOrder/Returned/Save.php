<?php
/**
 * Copyright © 2016 Magestore. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Magestore\PurchaseOrderSuccess\Controller\Adminhtml\PurchaseOrder\Returned;

/**
 * Class Save
 * @package Magestore\PurchaseOrderSuccess\Controller\Adminhtml\PurchaseOrder\Returned
 */
class Save extends \Magestore\PurchaseOrderSuccess\Controller\Adminhtml\AbstractAction
{
    /**
     * Authorization level of a basic admin session
     */
    const ADMIN_RESOURCE = 'Magestore_PurchaseOrderSuccess::returned_purchase_order';

    /**
     * @var \Magestore\PurchaseOrderSuccess\Service\PurchaseOrder\PurchaseOrderService
     */
    protected $purchaseOrderService;

    /**
     * @var \Magestore\PurchaseOrderSuccess\Service\PurchaseOrder\Item\Returned\ReturnedService
     */
    protected $returnedService;

    /**
     * @var \Magento\Framework\Stdlib\DateTime\TimezoneInterface
     */
    protected $timezone;

    /**
     * @var \Magento\Framework\ObjectManagerInterface
     */
    protected $_objectManager;

    public function __construct(
        \Magestore\PurchaseOrderSuccess\Controller\Adminhtml\Context $context,
        \Magestore\PurchaseOrderSuccess\Service\PurchaseOrder\PurchaseOrderService $purchaseOrderService,
        \Magestore\PurchaseOrderSuccess\Service\PurchaseOrder\Item\Returned\ReturnedService $returnedService,
        \Magento\Framework\Stdlib\DateTime\TimezoneInterface $timezone,
        \Magento\Framework\ObjectManagerInterface $objectManager
    ){
        parent::__construct($context);
        $this->purchaseOrderService = $purchaseOrderService;
        $this->returnedService = $returnedService;
        $this->timezone = $timezone;
        $this->_objectManager = $objectManager;
    }

    /**
     * Quotation grid
     *
     * @return \Magento\Backend\Model\View\Result\Page
     */
    public function execute()
    {
        $params = $this->getRequest()->getParams();
        $resultRedirect = $this->resultRedirectFactory->create();
        if(!$params['purchase_order_id']){
            $this->messageManager->addErrorMessage(__('Please select a purchase order to return product'));
            return $resultRedirect->setPath('*/purchaseOrder/');
        }

        if(!isset($params['dynamic_grid'])){
            $this->messageManager->addErrorMessage(__('Please return at least one product.'));
            return $resultRedirect->setPath('*/purchaseOrder/view',['id'=>$params['purchase_order_id']]);
        }
        $returnedData = $this->returnedService->processReturnedData($params['dynamic_grid']);
        try {
            $user = $this->_auth->getUser();
            $this->purchaseOrderService->returnProducts(
                $params['purchase_order_id'], $returnedData, $params['returned_at'], $user->getUserName()
            );
            $this->messageManager->addSuccessMessage(__('Return product(s) successfully.'));
        } catch (\Exception $e) {
            $this->messageManager->addErrorMessage($e->getMessage());
        }

        /*
         * <customization
         *      author="Mino"
         *      time="2017/05/04"
         *      content="Add integration with SerialSuccess" >
         */
        $serialSuccessEnabled = $this->_objectManager->create('Magento\Framework\Module\Manager')
            ->isEnabled('Magestore_InventorySuccess');
        if ($serialSuccessEnabled) {
            /** @var \Magestore\SerialSuccess\Model\ItemFactory $serialItemFactory */
            /** @var \Magestore\SerialSuccess\Model\TransactionFactory $serialTransFactory */
            /** @var \Magestore\SerialSuccess\Model\ResourceModel\Item\CollectionFactory $collectionFactory */
            $serialItemFactory = $this->_objectManager->create('Magestore\SerialSuccess\Model\ItemFactory');
            $serialTransFactory = $this->_objectManager->create('Magestore\SerialSuccess\Model\TransactionFactory');
            $collectionFactory = $this->_objectManager->create('Magestore\SerialSuccess\Model\ResourceModel\Item\CollectionFactory');
            $duplicate = 0;

            foreach ($params['dynamic_grid'] as &$p) {
                $deleteQty = sizeof($p['serial-data']) - $p['returned_qty'];
                if ($deleteQty > 0)
                    for ($i = 0; $i < $deleteQty; $i++)
                        array_pop($p['serial-data']);

                $serials = $collectionFactory->create()
                    ->addFieldToSelect('serial_item_id')
                    ->addFieldToSelect('serial')
                    ->addFieldToFilter('product_id', ['eq' => $p['id']]);
                $ids = $serials->getColumnValues('serial_item_id');
                $serials = $serials->getColumnValues('serial');

                foreach ($p['serial-data'] as $serial) {
                    $index = array_search($serial, $serials);
                    if ($index != null) {
                        $serialItemFactory->create()->load($ids[$index])->delete();
                        $transactionCollection = $serialTransFactory->create()->getCollection()
                            ->addFieldToFilter('serial_item_id', $ids[$index]);
                        foreach ($transactionCollection as $transaction) {
                            $transaction->delete();
                        }
                    }
                    else $duplicate++;
                }
            }
            if ($duplicate != 0)
                $this->messageManager->addNoticeMessage(
                    __('Warning! ' . $duplicate . ' serials cannot be removed as it cannot be found, you can manually add them later')
                );
        }
        // </customization>

        return $resultRedirect->setPath('*/purchaseOrder/view', ['id' => $params['purchase_order_id']]);
    }

}